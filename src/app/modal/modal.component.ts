import { Component, OnInit, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Model } from '../model/model.component'
import {Button} from '../model/buttonlist'
import {DropDown} from '../model/dropdownlist'
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

@Input() parentData :String;
@Output() submitStepper = new EventEmitter();


stepper2 = new Model().stepper_page_2;
stepper3 = new Model().stepper_page_3;
stepper4 = new Model().stepper_page_4;
stepper5 = new Model().stepper_page_5;
stepper6 = new Model().stepper_page_6;
userPage = new Model().user_page;

public stepper2form : FormGroup;
public stepper3form : FormGroup;
public stepper4form : FormGroup;
public stepper5form : FormGroup;
public stepper6form : FormGroup;
public userForm : FormGroup;
public user = "user";

formGroup: FormGroup;
buttonlist = new Button().button;
dropdownlist = new DropDown().dropdown;
emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"

  constructor(private fb:FormBuilder) {
    this.stepper2form = this.fb.group({});
    this.stepper3form = this.fb.group({});
    this.stepper4form = this.fb.group({});
    this.stepper5form = this.fb.group({});
    this.stepper6form = this.fb.group({});
    this.userForm = this.fb.group({});
   }

  ngOnInit() {
    this.createForm(this.stepper2,this.stepper2form);
    this.createForm(this.stepper3,this.stepper3form);
    this.createForm(this.stepper4,this.stepper4form);
    this.createForm(this.stepper5,this.stepper5form);
    this.createForm(this.stepper6,this.stepper6form);
    this.createForm(this.userPage,this.userForm);
  }

  createForm(array: Array<any>, form: FormGroup) {
    array.forEach(element => {
      if(element['email']){
      form.addControl(element['property'], new FormControl('', [ Validators.pattern(this.emailPattern), Validators.required]));   
      }
      else{
      form.addControl(element['property'], new FormControl('', Validators.required));
      }
    });
  }

  onSubmit(val) {
  this.submitStepper.emit(val);
  }

}


