import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  NewForm: FormGroup;
  public user_data = "user";
  constructor(private formBuilder: FormBuilder) {
    this.NewForm = this.formBuilder.group({
    });
   }

  ngOnInit() {
  }

}
