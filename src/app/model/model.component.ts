export class Model {
    stepper_page_2: any = [
    {
    label:"Workload type *",      //label to display
    type: "radio",    //type of control
    property: "workload", //from-control-name
    required: true,
    readonly: '!editable',
    reset: true,
    email : false,
    phone: false
    },
    {
    label:"Instance type *",
    type: "radio",
    property: "instance",
    required: true,
    readonly: '!editable',
    reset: true,
    email : false,
    phone: false
    
    },
    {
        label:"Instance Size *",
        type: "radio",
        property: "size",
        required: true,
        readonly: '!editable',
        reset: true,
        email : false,
        phone: false
        
    },
    {
        label:"Enter Volume Size *",
        type: "dropdown",
        property: "volume",
        required: true,
        readonly: '!editable',
        reset: true,
        email : false,
        phone: false
        
        }
    
    
    ]
    stepper_page_3: any = [
        {
            label:"Select Image *",      //label to display
            type: "radio",    //type of control
            property: "image", //from-control-name
            required: true,
            readonly: '!editable',
            reset: true,
            email : false,
            phone: false
        },
    ]
    stepper_page_4: any = [
        {
            label:"Enter User Friendly Cluster Name *",      //label to display
            type: "input",    //type of control
            property: "cluster", //from-control-name
            required: true,
            readonly: '!editable',
            reset: true,
            email : false,
            phone: false
        },
    ]
    stepper_page_5: any = [
        {
            label:"Select Shutdown Time*",      //label to display
            type: "radio",    //type of control
            property: "shutdown", //from-control-name
            required: true,
            readonly: '!editable',
            reset: true,
            email : false,
            phone: false
        },
    ]
    stepper_page_6: any = [
        {
            label:"Number of Nodes*",      //label to display
            type: "input",    //type of control
            property: "nodes", //from-control-name
            required: true,
            readonly: '!editable',
            reset: true,
            email : false,
            phone: false
        },
    ]
    user_page: any = [
        {
            label:"Product Owner*",      //label to display
            type: "dropdown",    //type of control
            property: "owner", //from-control-name
            required: true,
            readonly: '!editable',
            reset: true,
            email : false,
            phone: false
        },
        {
            label:"Name",      //label to display
            type: "input",    //type of control
            property: "userName", //from-control-name
            required: true,
            readonly: '!editable',
            reset: true,
            email : false,
            phone: false
        },
        {
            label:"Team Name",      //label to display
            type: "input",    //type of control
            property: "team", //from-control-name
            required: true,
            readonly: '!editable',
            reset: true,
            email : false,
            phone: false
        },
        {
            label:"Designation",      //label to display
            type: "input",    //type of control
            property: "designation", //from-control-name
            required: true,
            readonly: '!editable',
            reset: true,
            email : false,
            phone: false
        },
        {
            label:"Role",      //label to display
            type: "dropdown",    //type of control
            property: "role", //from-control-name
            required: true,
            readonly: '!editable',
            reset: true,
            email : false,
            phone: false
        },
        {
            label:"Phone",      //label to display
            type: "input",    //type of control
            property: "phone", //from-control-name
            required: true,
            readonly: '!editable',
            reset: true,
            email : false,
            phone: true
        },
        {
            label:"Email ID",      //label to display
            type: "input",    //type of control
            property: "email", //from-control-name
            required: true,
            readonly: '!editable',
            reset: true,
            email : true,
            phone: false
        },
    ]
    
}
    