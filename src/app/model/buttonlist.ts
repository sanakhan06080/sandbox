export class Button{
    button: any ={
        workload:['CPU','GPU'],
        instance:['Compute','Memory'],
        size:['Small','Medium','Large','XLarge'],
        image:['JPR','JPS','JPRS'],
        shutdown:['24','36','48']
    }
    
}