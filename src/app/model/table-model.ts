export class tableModel { 
    table1: any = [
        {
        name: "Aditi",
        clusterType: "Memory",
        launchTime: "Time",
        cost: "1000",
        state: "Active",
        nodes: "5",
        volume: "100",
        friendlyName :"dummy",
        // action: ["Play", "Pause", "Delete"],
        },
        {
            name: "Sana",
            clusterType: "Memory",
            launchTime: "Time",
            cost: "2000",
            state: "Active",
            nodes: "15",
            volume: "700",
            friendlyName :"dummy",
            // action: ["Play", "Pause", "Delete"],
        },
        {
            name: "Sahil",
            clusterType: "Memory",
            launchTime: "Time",
            cost: "500",
            state: "Stopped",
            nodes: "10",
            volume: "10",
            friendlyName :"dummy",
            // action: ["Play", "Pause", "Delete"],
        }
    ]
    table2: any = [
        {
        Name: "Jack Jones",
        Team: "QLabs",
        Project: "Qognition AI",
        Designation: "MLE",
        Role: "Developer",
        },
        {
        Name: "John Jacobs",
        Team: "QLabs",
        Project: "Driveboard",
        Designation: "BA",
        Role: "Viewer",
        },
        {
        Name: "Robert Joseph",
        Team: "QLabs",
        Project: "Quggle",
        Designation: "SA",
        Role: "Admin",
        },
    ]

}