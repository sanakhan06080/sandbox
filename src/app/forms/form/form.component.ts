import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {
  profileForm = this.formBuilder.group({
    name: ['', Validators.required],
    contact: ['',Validators.required],
    address: ['',Validators.required],
    password: ['',Validators.required]
  });

  constructor(private formBuilder: FormBuilder) {
   
  }


}
