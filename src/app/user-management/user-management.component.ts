import { Component, OnInit } from '@angular/core';
import {tableModel} from '../model/table-model'
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {
  form: FormGroup;
  table = new tableModel().table2;

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      userData: this.formBuilder.group({
      }),
    });
   }

  ngOnInit() {
  }

}
