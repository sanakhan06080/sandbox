import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormComponent } from './forms/form/form.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Form2Component } from './forms/form2/form2.component';
import { Form3Component } from './forms/form3/form3.component';
import{ReactiveFormsModule} from '@angular/forms';
import { StepperFormComponent } from './stepper-form/stepper-form.component';
import { ReactFormComponent } from './forms/react-form/react-form.component';
import { ModalComponent } from './modal/modal.component';
import { NewFormComponent } from './new-form/new-form.component';
import { DisplayModalComponent } from './display-modal/display-modal.component';
import { TableInfoComponent } from './table-info/table-info.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { AddUserComponent } from './add-user/add-user.component';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ng6-toastr-notifications';



@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    Form2Component,
    Form3Component,
    StepperFormComponent,
    ReactFormComponent,
    ModalComponent,
    NewFormComponent,
    DisplayModalComponent,
    TableInfoComponent,
    UserManagementComponent,
    AddUserComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ClarityModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
