import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class ClusterService {

  constructor(private http:HttpClient) {}

  head = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getUserCluster () {
    let data = { userId:"admin" , uuid: "wbzkrIba" };
    return this.http.post(`${environment.baseUrl}getUserClusters`, data, this.head)
  }

  getAllCluster () {
    let data = { userId:"admin" , uuid: "wbzkrIba" };
    return this.http.post(`${environment.baseUrl}getAllCluster`, data, this.head)

  }

  createCluster(d){
    let data = { 
    userId:"admin" , 
    type : d.type,

    workloadType : d.workload, 
    instanceType: d.instance,
    instanceSize: d.size,
    volumeSize : d.volume,
    image: d.image,
    nodeCount: d.nodes,
    userClusterName : d.cluster

    
  }
   console.log(data)
  return this.http.post(`${environment.baseUrl}createCluster`,data, this.head)
  
  }

  startCluster(d){
    let data = { userId:"admin" , uuid: "wbzkrIba" ,clusterName: d.name};
    // console.log(data)
    return this.http.post(`${environment.baseUrl}startCluster`, data, this.head)
   
  }

  stopCluster(d){
    let data = { userId:"admin" , uuid: "wbzkrIba" ,clusterName: d.name};
    // console.log(data)
    return this.http.post(`${environment.baseUrl}stopCluster`, data, this.head)
    
  }

  deleteCluster(d){
   let data = { userId:"admin" , uuid: "wbzkrIba" ,clusterName: d.name};
  //  console.log(data)  
   return this.http.post(`${environment.baseUrl}deleteCluster`, data, this.head)
    
  }
}
