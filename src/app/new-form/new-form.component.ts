import { Component, OnInit, Input, Output } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { templateJitUrl } from '@angular/compiler';

@Component({
  selector: 'app-new-form',
  templateUrl: './new-form.component.html',
  styleUrls: ['./new-form.component.css']
})
export class NewFormComponent implements OnInit {

  @Input() user :String;
  @Input('myForm') public myForm: FormGroup;
  @Input('formDetails') formDetails: Array<object>;
  @Input('buttonlist') buttonlist: Array<object>;
  @Input('dropdownlist') dropdownlist: Array<object>;
  @Output() submitForm = new EventEmitter();

  formGroup: FormGroup;
  selectedItems = [];

  constructor(public formBuilder: FormBuilder) { }
  

  ngOnInit() {
  }

  onSelectionChange(data ) {
   
     console.log(data['target']['id']);
    
}

onSubmit(post) {
   console.log(post);
   
  
  this.submitForm.emit(post);
  if(this.formDetails['reset']===true){
  this.myForm.controls[this.formDetails['property']].reset()
  }
  }
  

}
