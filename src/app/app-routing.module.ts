import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DisplayModalComponent } from './display-modal/display-modal.component';
import { AddUserComponent } from './add-user/add-user.component';
import { UserManagementComponent } from './user-management/user-management.component';


const routes: Routes = [
  // {path: '', component: UserManagementComponent },
  // {path: 'user', component: AddUserComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
