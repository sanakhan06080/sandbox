import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-display-modal',
  templateUrl: './display-modal.component.html',
  styleUrls: ['./display-modal.component.css']
})
export class DisplayModalComponent implements OnInit {

  @Input() show: boolean;
  @Output() showAgain = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }

  onShow(){
    this.show = false;
    this.showAgain.emit(this.show)
  }
}
