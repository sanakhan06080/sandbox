import { Component, OnInit } from '@angular/core';
import {ClusterService} from '../services/cluster.service';
import { ToastrManager } from 'ng6-toastr-notifications';


@Component({
  selector: 'app-table-info',
  templateUrl: './table-info.component.html',
  styleUrls: ['./table-info.component.css']
})
export class TableInfoComponent{

  table: any;
  headers : any;
  labels :any;
  showModal: any = false; 
  response : any;
  constructor(private _http:ClusterService, public toastr: ToastrManager) {
    this._http.getAllCluster().subscribe(
      res=>{
        console.log(res)
        this.table = res['data']
        this.headers = res['headers']
        this.labels = res['label']
      },
      err=>{}
    )
   }

  onClick(){
    this.showModal = true;
  }

  onShow(data){
    this.showModal = data;
  }

  play(data){
    console.log(data);
    this._http.startCluster(data).subscribe(
      res=>{
        console.log(res)
        this.response = res
        this.toastr.warningToastr(this.response);
      },
      err=>{}
    )
  }

  pause(data){
    console.log(data);
    this._http.stopCluster(data).subscribe(
      res=>{
        console.log(res)
        this.response = res
        this.toastr.warningToastr(this.response);
      },
      err=>{}
    )

  }

  delete(del, data ){
    this._http.deleteCluster(data).subscribe(
      res=>{
        console.log(res)
      },
      err=>{
        console.log(err)
      }
    )
    this.table.splice(del,1);
  }
}
