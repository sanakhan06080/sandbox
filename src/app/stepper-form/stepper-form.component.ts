import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { ClusterService } from '../services/cluster.service';

@Component({
  selector: 'app-stepper-form',
  templateUrl: './stepper-form.component.html',
  styleUrls: ['./stepper-form.component.css']
})
export class StepperFormComponent {

  form: FormGroup;
  public stepper_number = "step-2";
  public stepper_number3 = "step-3";
  public stepper_number4 = "step-4";
  public stepper_number5 = "step-5";
  public stepper_number6 = "step-6";
  public option;
  public stepper;
  public data: any = [];
  public obj ={};

  constructor(private formBuilder: FormBuilder,private _http:ClusterService) {
    this.form = this.formBuilder.group({
      stepper1: this.formBuilder.group({
        type:['' , Validators.required]
      }),
      stepper2: this.formBuilder.group({
      }),
      stepper3: this.formBuilder.group({
      }),
      stepper4: this.formBuilder.group({
      }),
      stepper5: this.formBuilder.group({
      }),
    });
  }

  buttonClick(value: string){
    this.option = value;
  }
  
  onSubmit(value){
    this.data.push(value['stepper1'])
  }
  
  submit(val) {
    console.log('stepper data here')
    this.data.push(val)
    console.log(this.data);
  }
  
  reset(){
    this.form.reset();
  }

  convert(){
      
     for(let detail of this.data){
      for (var key of Object.keys(detail)) {
        // console.log(key + " -> " + detail[key])
        this.obj[key] = detail[key]
        
      }
    } 
     console.log(this.obj)
    this._http.createCluster(this.obj).subscribe(
      res=>{
        console.log(res)
      },
      err=>{
        console.log(err)
      }
    )
  }
}
